<?php
namespace App\Command;

use App\Service\ClassHelper;
use App\Service\EmployeeFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckResponsibility extends Command
{
    protected static $defaultName = 'employee:can';
    /**
     * @var EmployeeFactory
     */
    private $employeeFactory;
    /**
     * @var ClassHelper
     */
    private $classHelper;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ShowEmployeeSkillsCommand constructor.
     * @param EmployeeFactory $employeeFactory
     * @param ClassHelper $classHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        EmployeeFactory $employeeFactory,
        ClassHelper $classHelper,
        LoggerInterface $logger)
    {
        parent::__construct(null);
        $this->employeeFactory = $employeeFactory;
        $this->classHelper = $classHelper;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Check employee responsibility')
            ->addArgument('position', InputArgument::REQUIRED, 'The position name.')
            ->addArgument('responsibility', InputArgument::REQUIRED, 'The responsibility name.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $position = (string)$input->getArgument("position");
        $responsibility = (string)$input->getArgument("responsibility");

        $employee = $this->employeeFactory::createEmployee($position);

        $output->writeln($this->classHelper->hasMethod($employee, $responsibility));
    }
}