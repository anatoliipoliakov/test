<?php
namespace App\Command;

use App\Service\ClassHelper;
use App\Service\EmployeeFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShowResponsibilities extends Command
{
    protected static $defaultName = 'company:employee';
    /**
     * @var EmployeeFactory
     */
    private $employeeFactory;
    /**
     * @var ClassHelper
     */
    private $classHelper;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ShowEmployeeSkillsCommand constructor.
     * @param EmployeeFactory $employeeFactory
     * @param ClassHelper $classHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        EmployeeFactory $employeeFactory,
        ClassHelper $classHelper,
        LoggerInterface $logger)
    {
        parent::__construct(null);
        $this->employeeFactory = $employeeFactory;
        $this->classHelper = $classHelper;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Show employee responsibilities')
            ->addArgument('position', InputArgument::REQUIRED, 'The position name.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $position = (string)$input->getArgument("position");
        try {
            $employee = $this->employeeFactory::createEmployee($position);
            $this->classHelper->executeAllMethods($employee);
        } catch (\Exception $e) {
            $this->logger->error("Error in command", [
                "class" => get_class($e),
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
            ]);
        }
    }
}