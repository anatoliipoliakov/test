<?php
namespace App\Service;

use App\Entity\Designer;
use App\Entity\Manager;
use App\Entity\Programmer;
use App\Entity\Tester;

class EmployeeFactory
{
    public static function createEmployee(string $position)
    {
        switch ($position) {
            case 'programmer':
                return new Programmer();
                break;
            case 'tester':
                return new Tester();
                break;
            case 'designer':
                return new Designer();
                break;
            case 'manager':
                return new Manager();
                break;
            default:
                throw new \Exception('Wrong position passed.');
        }
    }
}