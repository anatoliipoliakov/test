<?php
namespace App\Service;

class ClassHelper
{
    public function executeAllMethods($class)
    {
        $methods = get_class_methods($class);

        foreach ($methods as $method) {
            $class->$method();
        }
    }

    public function hasMethod($class, $methodName): string
    {
        return method_exists($class, $methodName) ? 'true' : 'false';
    }
}