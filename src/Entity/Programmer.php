<?php
namespace App\Entity;

use App\Entity\ProgrammerInterface;
use App\Entity\TesterInterface;
use App\Entity\CommunicationInterface;

class Programmer implements ProgrammerInterface, TesterInterface, CommunicationInterface
{
    public function writeCode()
    {
        echo "- code writing" . PHP_EOL;
    }

    public function test()
    {
        echo "- code testing" . PHP_EOL;
    }

    public function communicate()
    {
        echo "- communication with manager" . PHP_EOL;
    }
}