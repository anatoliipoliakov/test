<?php
namespace App\Entity;

use App\Entity\DesignerInterface;
use App\Entity\CommunicationInterface;

class Designer implements DesignerInterface, CommunicationInterface
{
    public function draw()
    {
        echo "- drawing" . PHP_EOL;
    }

    public function communicate()
    {
        echo "- communication with manager" . PHP_EOL;
    }
}