<?php
namespace App\Entity;

use App\Entity\TesterInterface;
use App\Entity\CommunicationInterface;
use App\Entity\TaskInterface;

class Tester implements TesterInterface, CommunicationInterface, TaskInterface
{
    public function test()
    {
        echo "- code testing" . PHP_EOL;
    }

    public function communicate()
    {
        echo "- communication with manager" . PHP_EOL;
    }

    public function setTask()
    {
        echo "- setting task" . PHP_EOL;
    }
}