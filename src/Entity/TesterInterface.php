<?php
namespace App\Entity;

interface TesterInterface
{
    public function test();
}
