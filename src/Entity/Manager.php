<?php
namespace App\Entity;

use App\Entity\TaskInterface;

class Manager implements TaskInterface
{
    public function setTask()
    {
        echo "- setting task" . PHP_EOL;
    }
}