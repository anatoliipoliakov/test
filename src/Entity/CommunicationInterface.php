<?php
namespace App\Entity;

interface CommunicationInterface
{
    public function communicate();
}
