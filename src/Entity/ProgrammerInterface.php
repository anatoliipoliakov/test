<?php
namespace App\Entity;

interface ProgrammerInterface
{
    public function writeCode();
}
