<?php
namespace App\Entity;

interface DesignerInterface
{
    public function draw();
}
